# svc-name-generator
I'm terrible at naming things, so I wrote a service to do it for me.

### What the _[preferred expletive here]_ is this?
This is a mix of various open-source components thrown together in a rush so I could keep working on infrastructure design.

Serious answer: An API that runs on [Fastly's](https://fastly.com) Compute@Edge platform allowing me to generate random Japanese names to use for different parts of my infrastructure & projects.

At some point, this may be extended to allow for additional names (e.g: actors, musicians, household items), but for now "It Just Works"™.

### This exists thanks to the following amazing libraries
 - [rust-gimei](https://github.com/woxtu/rust-gimei) - a Rust port of the original [Gimei](https://github.com/willnet/gimei).
 - [wana_kana_rust](https://github.com/PSeitz/wana_kana_rust) - a Rust port of the original [WanaKana](https://github.com/WaniKani/WanaKana).
use fastly::http::{header, Method, StatusCode};
use fastly::{mime, Error, Request, Response};

#[fastly::main]
fn main(req: Request) -> Result<Response, Error> {
    match req.get_method() {
        &Method::GET | &Method::HEAD => (),

        _ => {
            return Ok(Response::from_status(StatusCode::METHOD_NOT_ALLOWED)
                .with_header(header::ALLOW, "GET, HEAD")
                .with_body_text_plain("method not allowed"))
        }
    };

    let mut name = gimei::Name{
        first: gimei::Item {
            kanji: "".to_string(),
            hiragana: "".to_string(),
            katakana: "".to_string(),
        },
        last: gimei::Item {
            kanji: "".to_string(),
            hiragana: "".to_string(),
            katakana: "".to_string(),
        },
        gender: gimei::Gender::Male,
    };
    let mut final_name = "".to_string();
    let mut not_found = false;

    match req.get_path() {
        "/" | "/romaji" => {
            name = gimei::name();
            final_name = wana_kana::to_romaji::to_romaji(name.first.hiragana.as_str());
        }

        "/kanji" => {
            name = gimei::name();
            final_name = name.first.kanji.to_string();
        }

        "/hiragana" => {
            name = gimei::name();
            final_name = name.first.hiragana.to_string();
        }

        "/katakana" => {
            name = gimei::name();
            final_name = name.first.katakana.to_string();
        }

        "/male" | "/male/romaji" => {
            name = gimei::male();
            final_name = wana_kana::to_romaji::to_romaji(name.first.hiragana.as_str());
        }

        "/male/kanji" => {
            name = gimei::male();
            final_name = name.first.kanji.to_string();
        }

        "/male/hiragana" => {
            name = gimei::male();
            final_name = name.first.hiragana.to_string();
        }

        "/male/katakana" => {
            name = gimei::male();
            final_name = name.first.katakana.to_string();
        }

        "/female" | "/female/romaji" => {
            name = gimei::female();
            final_name = wana_kana::to_romaji::to_romaji(name.first.hiragana.as_str());
        }

        "/female/kanji" => {
            name = gimei::female();
            final_name = name.first.kanji.to_string();
        }

        "/female/hiragana" => {
            name = gimei::female();
            final_name = name.first.hiragana.to_string();
        }

        "/female/katakana" => {
            name = gimei::female();
            final_name = name.first.katakana.to_string();
        }

        // Catch all other requests and return a 404.
        _ => not_found = true
    }

    if not_found {
        return Ok(Response::from_status(StatusCode::NOT_FOUND)
            .with_body_text_plain("not found"))
    }

    Ok(Response::from_status(StatusCode::OK)
        .with_content_type(mime::TEXT_PLAIN_UTF_8)
        .with_body(final_name))
}
